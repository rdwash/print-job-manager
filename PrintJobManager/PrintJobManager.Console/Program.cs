﻿using System.Collections.Generic;
using PrintJobManager.Services.Services;
using PrintJobManager.Services.BizObjects;

namespace PrintJobManager.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            IList<Job> SampleJobs = new List<Job>();
            ICostCalculator costCalculator = new PrintJobCostCalculator();

            SampleJobs = SampleData.CreateSampleData();

            //If using a file:
            /*
            const string dataDirectory = @"..\\..\\data\";            
            string[] files = Directory.GetFiles(dataDirectory);
            IFileProcessor fp = new JobFileProcessor();            
            SampleJobs = fp.ReadFile(files[0]);
            */

            JobManager jobManager = new JobManager(SampleJobs);

            jobManager.RegisterCostCalculator(costCalculator);
            jobManager.CalculateTotals();
            jobManager.DisplayJobInvoiceReport();

            //If using a file:
            jobManager.CreateJobInvoiceFile();

            System.Console.ReadKey();
        }
    }
}