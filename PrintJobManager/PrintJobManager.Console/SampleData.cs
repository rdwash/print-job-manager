﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrintJobManager.Services.BizObjects;

namespace PrintJobManager.Console
{
    public static class SampleData
    {
        public static IList<Job> SampleJobs = new List<Job>();

        public static IList<Job> CreateSampleData()
        {
            Job job1 = new Job()
            {
                JobName = "Job 1",
                ExtraMargin = true
            };
            var job1_jobItem1 = new JobItem()
            {
                ItemName = "Envelopes",
                ItemCost = 520.00M,
                Exempt = false
            };
            var job1_jobItem2 = new JobItem()
            {
                ItemName = "Letterhead",
                ItemCost = 1983.37M,
                Exempt = true
            };
            job1.AddNewJobItem(job1_jobItem1);
            job1.AddNewJobItem(job1_jobItem2);
            SampleJobs.Add(job1);

            /////////////////////////////////////////////////////////

            Job job2 = new Job()
            {
                JobName = "Job 2",
                ExtraMargin = false
            };
            var job2_jobItem1 = new JobItem()
            {
                ItemName = "T-Shirts",
                ItemCost = 294.04M,
                Exempt = false
            };
            job2.AddNewJobItem(job2_jobItem1);
            SampleJobs.Add(job2);

            /////////////////////////////////////////////////////////

            Job job3 = new Job()
            {
                JobName = "Job 3",
                ExtraMargin = true
            };
            var job3_jobItem1 = new JobItem()
            {
                ItemName = "Frisbees",
                ItemCost = 19385.38M,
                Exempt = true
            };
            var job3_jobItem2 = new JobItem()
            {
                ItemName = "Yo-Yos",
                ItemCost = 1829.00M,
                Exempt = true
            };
            job3.AddNewJobItem(job3_jobItem1);
            job3.AddNewJobItem(job3_jobItem2);
            SampleJobs.Add(job3);

            return SampleJobs;
        }
    }
}
