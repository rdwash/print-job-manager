﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrintJobManager.Services.BizObjects;

namespace PrintJobManager.Services.Services
{
    public class JobManager
    {
        public IList<Job> CurrentJobs { get; set; }
        public ICostCalculator CostCalculator { get; set; }
        public IFileProcessor FileProcessor { get; set; }

        public JobManager(IList<Job> jobs)
        {
            CurrentJobs = jobs;
            FileProcessor = new JobFileProcessor();
        }

        public void AddNewJob(Job newJob)
        {
            CurrentJobs.Add(newJob);
        }

        public void RegisterCostCalculator(ICostCalculator costCalculator)
        {
            CostCalculator = costCalculator;
        }
        
        public void CalculateTotals()
        {
            foreach(Job job in CurrentJobs)
            {
                CostCalculator.CaculateItemGroupCost(job.JobItems);
                CostCalculator.CalculateJobCost(job);
            }
        }

        public void DisplayJobInvoiceReport()
        {
            foreach(Job job in CurrentJobs)
            {
                Console.WriteLine(job.ToString());
                foreach(JobItem jobItem in job.JobItems)
                {
                    Console.WriteLine(jobItem.ToString());
                }

                Console.Write("\n\n");
            }
        }

        public void CreateJobInvoiceFile()
        {
            StringBuilder sb = new StringBuilder();

            foreach (Job job in CurrentJobs)
            {
                sb.AppendLine(job.ToString());
                foreach (JobItem jobItem in job.JobItems)
                {
                    sb.AppendLine(jobItem.ToString());
                }

                sb.AppendLine().AppendLine();
            }

            FileProcessor.WriteFile(sb.ToString());
        }
    }
}