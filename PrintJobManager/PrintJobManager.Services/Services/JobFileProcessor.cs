﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using PrintJobManager.Services.BizObjects;
using PrintJobManager.Services.Types;

namespace PrintJobManager.Services.Services
{
    public class JobFileProcessor : IFileProcessor
    {
        public List<Job> finalJobs = new List<Job>();
        public const string invoiceFileLocation = @"..\\..\\data\";
        public string invoiceFileName = string.Format("print-job-invoices-{0}{1}{2}.txt", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year);

        public List<Job> ReadFile(string fileLocation)
        {
            if (File.Exists(fileLocation))
            {
                using (StreamReader reader = new StreamReader(fileLocation))
                {
                    string record = string.Empty;
                    while ((record = reader.ReadLine()) != null)
                    {
                        //TODO: Create a file schema from which to control how data is loaded from the file
                        foreach (KeyValuePair<string, char> pair in DelimeterTypes.DelimeterTypesDict)
                        {
                            if (record.Contains(pair.Value))
                            {
                                LoadFileData(record, pair.Value);
                            }
                        }
                    }
                }
            }

            return finalJobs;
        }        

        public void WriteFile(string data)
        {
            if (File.Exists(invoiceFileLocation + invoiceFileName))
                File.Delete(invoiceFileLocation + invoiceFileName);

            File.WriteAllText(invoiceFileLocation + invoiceFileName, data);
        }

        public void LoadFileData(string record, char delimeter)
        {
            string[] recordData = record.Split(delimeter);
            Job newJob = new Job();
            //TODO: Load Job Data from file into new object

            finalJobs.Add(newJob);
        }
    }
}