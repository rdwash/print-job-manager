﻿using System.Collections.Generic;
using PrintJobManager.Services.BizObjects;

namespace PrintJobManager.Services.Services
{
    public interface IFileProcessor
    {
        List<Job> ReadFile(string fileLocation);
        void WriteFile(string data);
    }
}