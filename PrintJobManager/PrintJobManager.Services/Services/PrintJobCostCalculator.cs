﻿using System;
using System.Collections.Generic;
using PrintJobManager.Services.BizObjects;

namespace PrintJobManager.Services.Services
{
    public class PrintJobCostCalculator : ICostCalculator
    {
        public const int BASE_MARGIN_PERCENTAGE = 11;
        public const int EXTRA_MARGIN_PERCENTAGE = 5;
        public const int BASE_EXTRA_MARGIN_PERCENTAGE = BASE_MARGIN_PERCENTAGE + EXTRA_MARGIN_PERCENTAGE;
        public const int JOB_ITEM_SALES_TAX = 7;

        public PrintJobCostCalculator() { }

        public void CaculateItemGroupCost(IList<JobItem> jobItems)
        {
            if (jobItems == null || jobItems.Count <= 0)
                throw new NullReferenceException("Job Items collection reference is null.");

            foreach(JobItem jobItem in jobItems)
            {
                CalculateItemCostViaSalesTax(jobItem);
            }
        }

        public void CalculateItemCost(JobItem jobItem)
        {
            if (jobItem == null)
                throw new NullReferenceException("Job Item reference is null.");

            CalculateItemCostViaSalesTax(jobItem);
        }        

        public void CalculateJobCost(Job job)
        {
            if (job == null)
                throw new NullReferenceException("Job reference is null.");

            if (job.ExtraMargin)
            {
                CalculateJobCostViaExtraMargin(job);
            }
            else
                CalculateJobCostViaBaseMargin(job);
        }

        private void CalculateJobCostViaBaseMargin(Job job)
        {
            CalculateJobBaseCost(job);

            job.TotalJobCost = decimal.Round(
                                job.BaseJobCost + (decimal.Multiply(job.BaseJobCost, BASE_MARGIN_PERCENTAGE) / 100)
                                , 2
                                , MidpointRounding.ToEven);
        }

        private void CalculateJobCostViaExtraMargin(Job job)
        {
            CalculateJobBaseCost(job);

            job.TotalJobCost = decimal.Round(
                                job.BaseJobCost + (decimal.Multiply(job.BaseJobCost, BASE_EXTRA_MARGIN_PERCENTAGE) / 100)
                                , 2
                                , MidpointRounding.ToEven);
        }

        private void CalculateJobBaseCost(Job job)
        {
            foreach (JobItem jobItem in job.JobItems)
            {
                job.BaseJobCost += jobItem.TotalItemCost;
            }
        }

        private void CalculateItemCostViaSalesTax(JobItem jobItem)
        {
            jobItem.TotalItemCost = jobItem.Exempt
                                     ? decimal.Round(jobItem.ItemCost, 2)
                                     : decimal.Round(jobItem.ItemCost + (decimal.Multiply(jobItem.ItemCost, JOB_ITEM_SALES_TAX) / 100), 2);
        }
    }
}