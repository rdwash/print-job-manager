﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrintJobManager.Services.BizObjects;

namespace PrintJobManager.Services.Services
{
    public interface ICostCalculator
    {
        void CalculateItemCost(JobItem jobItem);
        void CaculateItemGroupCost(IList<JobItem> jobItems);
        void CalculateJobCost(Job job);
    }
}