﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintJobManager.Services.BizObjects
{
    public class JobItem
    {
        public string ItemName { get; set; }
        public decimal ItemCost { get; set; }
        public bool Exempt { get; set; }
        public decimal TotalItemCost { get; set; }

        public JobItem() {
                
        }
        
        public override string ToString()
        {
            return string.Format("Item: {0} | Base Cost: ${1} | Final Cost: ${2}", ItemName, ItemCost, TotalItemCost);
        }
    }
}