﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintJobManager.Services.BizObjects
{
    public class Job
    {
        public string JobName { get; set; }
        public IList<JobItem> JobItems { get; set; }
        public decimal BaseJobCost { get; set; }
        public decimal TotalJobCost { get; set; }
        public bool ExtraMargin { get; set; }

        public Job() {
            JobItems = new List<JobItem>();
        }

        public void AddNewJobItem(JobItem jobItem)
        {
            JobItems.Add(jobItem);
        }

        public override string ToString()
        {
            return String.Format("Job: {0} | Base Cost: ${1} | Final Cost: ${2}", JobName, BaseJobCost, TotalJobCost);
        }
    }
}