# PRINT JOB MANAGER
A Job is a group of print items. For example, a job can be a run of:  
* Business Cards
* Envelopes  
* Letterhead

### Sales Tax (job items):
* Default: 7%
* Exempt Status: items that qualify as being sales tax free

### Margin (jobs):
* Base Margin: 11%
* Extra Margin: additional 5% (16% total)

### Cost:
* Final Cost: rounded to the nearest "even" cent.
* Item Cost: rounded to the nearest cent.

### Cost & Margin Example Specs:
* Item: $100 + $7 (sales tax) = $107
* Job (consisting of just the one item above): $100 + $11 (margin) = $111
* Total: $100 + $7 + $11 = $118

### Objective:
Write a program that calculates the total charge to a customer for a job. The program should accept the 
inputs below and output the total bill for the customer.  
* Bonus: Read the input from a file and output the invoice to a file.
* Bonus: Implement IoC and Unit Tests.

### Programming Language: C#

### Examples: 
#### Input -
Job 1:  
extra-margin  
envelopes 520.00  
letterhead 1983.37 exempt  

#### Output - 
envelopes: $556.40  
letterhead: $1983.37  
total: $2940.30  

#### Input - 
Job 2:  
t-shirts: 294.04  

#### Output - 
t-shirts: $314.62  
total: $346.96  

#### Input - 
Job 3:  
extra-margin  
frisbees 19385.38 exempt  
yo-yos 1829.00 exempt  

#### Output - 
frisbees: $19385.38  
yo-yos: $1829.00  
total: $24608.68  
